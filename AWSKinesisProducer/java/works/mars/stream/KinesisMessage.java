package works.mars.stream;

import java.nio.ByteBuffer;

/**
 * Immutable message class to be used for messaging between clients
 * 
 * @author marz1
 * @version 0.1
 */
public class KinesisMessage {

	/**
	 * The partition key to use
	 */
	private String channelIdentifier;

	/**
	 * The payload containing the data sent to Kinesis
	 */
	private ByteBuffer data;

	/**
	 * @param partitionKey
	 *            The partition key
	 * @param data
	 *            The byte buffer
	 */
	public KinesisMessage(String partitionKey, String data) {
		super();
		this.channelIdentifier = partitionKey;
		this.data = ByteBuffer.wrap(data.getBytes());
	}

	/**
	 * @return The partition key
	 */
	public String getPartitionKey() {
		return channelIdentifier;
	}

	/**
	 * @return The data payload
	 */
	public ByteBuffer getData() {
		return data;
	}

}

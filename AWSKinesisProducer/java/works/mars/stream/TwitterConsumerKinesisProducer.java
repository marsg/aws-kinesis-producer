package works.mars.stream;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Properties;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.kinesis.AmazonKinesis;
import com.amazonaws.services.kinesis.AmazonKinesisClient;
import com.twitter.hbc.ClientBuilder;
import com.twitter.hbc.core.Client;
import com.twitter.hbc.core.Constants;
import com.twitter.hbc.core.endpoint.StatusesFilterEndpoint;
import com.twitter.hbc.core.processor.StringDelimitedProcessor;
import com.twitter.hbc.httpclient.auth.Authentication;
import com.twitter.hbc.httpclient.auth.OAuth1;

/**
 * Gets messages from the Twitter filtered stream, and puts them into Kinesis Stream
 
 * @author marz1
 * @version 0.1
 *
 */
public class TwitterConsumerKinesisProducer {

	// AWS Credentials
	private static final String DEFAULT_PROP_FILE_NAME = "AwsUserData";

	// Twitter credentials and settings
	private final String TWIT_SECRET = "twitter.secret";
	private final String TWIT_TOKEN = "twitter.token";
	private final String TWIT_CONSUMER_SECRET = "twitter.consumerSecret";
	private final String TWIT_CONSUMER_KEY = "twitter.consumerKey";
	private final String HASHTAGS = "twitter.hashtags";

	// Kinesis Credentials
	private static final String REGION_NAME = "aws.regionName";
	private final String STREAM_NAME = "aws.streamName";

	private static final Log LOG = LogFactory.getLog(TwitterConsumerKinesisProducer.class);
	

	public static void main(String[] args) {

		TwitterConsumerKinesisProducer driver = new TwitterConsumerKinesisProducer();
		String arg = args.length == 1 ? args[0] : null;
		driver.getProgProperties(arg);
		driver.run();
	}

	private void getProgProperties(String filePath) {
		try {

			if (filePath == null) {
				String userHome = System.getProperty("user.home");
				filePath = userHome + "/" + DEFAULT_PROP_FILE_NAME + ".properties";
			}

			InputStream fileStream = new FileInputStream(filePath);

			Properties p = new Properties(System.getProperties());
			p.load(fileStream);
			System.setProperties(p);

			// got them from the file
			LOG.info("Program properties retrieval: success");
		} catch (FileNotFoundException e) {
			// file is not there...
		} catch (IOException e) {
			// can't read file
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void run() {

		String consumerKey = System.getProperty(TWIT_CONSUMER_KEY);
		String consumerSecret = System.getProperty(TWIT_CONSUMER_SECRET);
		String token = System.getProperty(TWIT_TOKEN);
		String secret = System.getProperty(TWIT_SECRET);
		String streamName = System.getProperty(STREAM_NAME);
		String regionName = System.getProperty(REGION_NAME);
		String filterTerms = System.getProperty(HASHTAGS);

		while (true) {
			/**
			 * Set up your blocking queues: Be sure to size these properly based
			 * on expected TPS of your stream
			 */
			BlockingQueue<String> msgQueue = new LinkedBlockingQueue<String>(10000);

			// Twitter connection
			StatusesFilterEndpoint endpoint = new StatusesFilterEndpoint();
			endpoint.trackTerms(Arrays.asList(filterTerms.split("\\s*,\\s*")));
			Authentication hosebirdAuth = new OAuth1(consumerKey, consumerSecret, token, secret);
			Client client = new ClientBuilder().hosts(Constants.STREAM_HOST).endpoint(endpoint)
					.authentication(hosebirdAuth).processor(new StringDelimitedProcessor(msgQueue)).build();

			client.connect();
			LOG.info("Connected to Twitter filtered stream...");

			// Kinesis connection
			AmazonKinesis kinesisClient = new AmazonKinesisClient(); 
			kinesisClient.setRegion(Region.getRegion(Regions.fromName(regionName)));
			KinesisStreamProducer producer = new KinesisStreamProducer("TwitterStreamProducer", streamName, 10,
					kinesisClient);
			producer.connect();
			LOG.info("Got connection to Kinesis");

			// start processing
			try {
				if (process(msgQueue, producer)) {
					break;
				}

			} catch (Exception e) {
				
				e.printStackTrace();
			}
			
			client.stop();
		}
	}

	private boolean process(BlockingQueue<String> msgQueue, KinesisStreamProducer producer) {

		int exceptionCount = 0;
		while (true) {
			try {
				
				String msg = msgQueue.take();
				String key = String.valueOf(System.currentTimeMillis());
				producer.post(key, msg);

			} catch (Exception e) {
				
				e.printStackTrace();

				if (++exceptionCount > 5) {
					
					return false;
				}
			}
		}

	}

}

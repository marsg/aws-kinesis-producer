package works.mars.stream;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicBoolean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.kinesis.AmazonKinesis;

/**
 * Handles the worker threads
 * 
 * @author marz1
 *
 */
public class KinesisStreamProducer {

	private ExecutorService executorService;

	private final int numberOfThreads;

	// only one kinesis stream producer can be operating
	private final AtomicBoolean canRun;

	private final String producerName;

	private final String streamNameToPost;

	private final AmazonKinesis kinesisClient;

	private final BlockingQueue<KinesisMessage> eventsQueue;

	private final static Logger logger = LoggerFactory.getLogger(KinesisStreamProducer.class);

	/**
	 * @param kinesisStreamProducerName
	 *            The name of the client, used for debugging purposes
	 * @param streamNameToPost
	 *            The name of the stream to send data to
	 * @param numberOfThreads
	 *            The number of threads to put in the pool
	 * @param kinesisClient
	 *            Amazon Kinesis Stream Client
	 */
	public KinesisStreamProducer(String kinesisStreamProducerName, String streamNameToPost, int numberOfThreads,
			AmazonKinesis kinesisClient) {

		eventsQueue = new LinkedBlockingQueue<KinesisMessage>();
		this.producerName = kinesisStreamProducerName;
		this.canRun = new AtomicBoolean(true);
		this.numberOfThreads = numberOfThreads;
		this.streamNameToPost = streamNameToPost;
		this.kinesisClient = kinesisClient;
	}

	public void connect() {

		if (!canRun.compareAndSet(true, false)) {
			throw new IllegalStateException("Already running");
		}

		ThreadFactory threadFactory = Executors.defaultThreadFactory();
		executorService = Executors.newFixedThreadPool(numberOfThreads, threadFactory);

		for (int i = 0; i < numberOfThreads; i++) {
			ProducerWorker p = new ProducerWorker(eventsQueue, kinesisClient, streamNameToPost);
			executorService.execute(p);
			logger.info(producerName + ": New thread started : {}", p);
		}

	}

	public void stop() {
		logger.info("Stopping the client");
		try {
			executorService.shutdownNow();

			logger.info(producerName + ": Successfully stopped the kinesis stream producer");
		} catch (Exception e) {
			logger.info(producerName + ": Exception when attempting to stop the kinesis stream producer: "
					+ e.getMessage());
		}
	}

	public void post(String partitionKey, String data) {
		KinesisMessage event = new KinesisMessage(partitionKey, data);
		eventsQueue.offer(event);
	}

}

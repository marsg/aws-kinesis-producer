package works.mars.stream;

import java.util.concurrent.BlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.kinesis.AmazonKinesis;
import com.amazonaws.services.kinesis.model.PutRecordRequest;
import com.amazonaws.services.kinesis.model.PutRecordResult;

/*
 * Consumes messages from the queue, and sends them to kinesis stream
 * @author marz1
 * @version 0.1
 */
public class ProducerWorker implements Runnable {

	private final BlockingQueue<KinesisMessage> eventsQueue;
	private final AmazonKinesis kinesisClient;
	private final String streamName;

	private final static Logger logger = LoggerFactory.getLogger(ProducerWorker.class);

	public ProducerWorker(BlockingQueue<KinesisMessage> eventsQueue, AmazonKinesis kinesisClient, String streamName) {
		this.eventsQueue = eventsQueue;
		this.kinesisClient = kinesisClient;
		this.streamName = streamName;

	}

	public void run() {

		while (true) {
			try {
				// get message from queue - blocking so code will wait here for
				// work to do
				KinesisMessage event = eventsQueue.take();
				PutRecordRequest put = new PutRecordRequest();

				put.setStreamName(this.streamName);
				put.setData(event.getData());
				put.setPartitionKey(event.getPartitionKey());

				PutRecordResult result = kinesisClient.putRecord(put);

				logger.info(result.getSequenceNumber() + ": {}", this);

			} catch (Exception e) {
				// didn't get record - move on to next\
				e.printStackTrace();
			}
		}

	}
}
